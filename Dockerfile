FROM java:8

ADD target/dockerdemo-2.0.jar dockerdemo-2.0.jar

EXPOSE 8080

ENTRYPOINT [ "java", "-jar", "dockerdemo-2.0.jar" ]
